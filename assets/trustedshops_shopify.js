// Version 1.2
// console.log("Trusted Shops Badge", "loaded: assets/trustedshops_shopify.js");
(function () { 
var _tsid = 'XFD9974BBC558C007CD46431D056DF230'; 
_tsConfig = { 
    'yOffset': '', /* offset from page bottom */
    'variant': 'reviews', /* text, default, small, reviews, custom, custom_reviews */
    'disableTrustbadge': '' /* deactivate trustbadge */
};
var _ts = document.createElement('script');
_ts.type = 'text/javascript'; 
_ts.charset = 'utf-8'; 
_ts.async = true; 
_ts.src = '//widgets.trustedshops.com/js/' + _tsid + '.js'; 
var __ts = document.getElementsByTagName('script')[0];
__ts.parentNode.insertBefore(_ts, __ts);
})();(function () {
  // Version 1.1
  // console.log("productScript", "loaded");
  var tsId = 'XFD9974BBC558C007CD46431D056DF230';
  var tsLocale = 'en_GB';
  var loadProductReviews = function(tsId, skus) {
    _tsProductReviewsConfig = {
      tsid: tsId,
      locale: tsLocale,        
      sku: skus,
      variant: 'productreviews',
      borderColor: '#333333',
      // Correct: Test override
      //'apiServer': '//api-qa.trustedshops.com/',
      introtext: ''
    };
    //var scripts = document.getElementsByTagName('SCRIPT'), me = scripts[scripts.length - 1];
    var me = document.getElementById('product');
    var _ts = document.createElement('SCRIPT');
    _ts.type = 'text/javascript';
    _ts.async = true;
    _ts.charset = 'utf-8';
    // Correct: Test override
    _ts.src ='//widgets.trustedshops.com/reviews/tsSticker/tsProductSticker.js'; 
    //_ts.src ='//qa.trustedshops.com/trustbadge/reviews/tsSticker/tsProductSticker.js';
    me.appendChild(_ts); 
    _tsProductReviewsConfig.script = _ts;
  };
  var pathname = window.location.pathname;
  if (pathname.indexOf('/products/') !== -1) {
    // console.log("productScript", "on product page");
    var request = new XMLHttpRequest();
    var productUrl = window.location.pathname+'.js';
    request.open('GET', productUrl, true);
    request.onload = function() {
      if (request.status >= 200 && request.status < 400) {
        // Success!
        var data = JSON.parse(request.responseText);
        // extract skus
        var skus = [];
        var arrayLength = data.variants.length;
        for (var i = 0; i < arrayLength; i++) {
            // console.log(data.variants[i]);
            skus.push(data.variants[i].sku);
        }
        // console.log("success", skus, data);
        // Correct: Test override
        loadProductReviews(tsId, skus);
      } else {
        // We reached our target server, but it returned an error
        // console.warn("Could not load product json", request.status);
      }
    };

    request.onerror = function(error) {
      // There was a connection error of some sort
      // console.warn("Error loading product json", productUrl, error);
    };
    request.send();
  };
})();